package tradebook.mongo.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import tradebook.mongo.repository.TradeRepository;

@RunWith(MockitoJUnitRunner.class)
public class TradeServiceTest {
	
	@Mock
	TradeRepository tradeRepository;
	
	@Test
	public void TestFindAll() {
		assertNotNull(tradeRepository.findAll());
	}

}

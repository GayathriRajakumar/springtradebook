package tradebook.mongo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import tradebook.mongo.Application;
import tradebook.mongo.model.Trade;
import tradebook.mongo.model.TradeType;

import static org.junit.Assert.*;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TradeAngularControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void contextLoads() {

    }

    @Test
    public void testGetAllTrades() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/trades",
                HttpMethod.GET, entity, String.class);

        assertNotNull(response.getBody());
    }

    @Test
    public void testGetTradeById() {
        Trade trade = restTemplate.getForObject(getRootUrl() + "/trades/1", Trade.class);
        assertNotNull(trade);
    }

    @Test
    public void testCreateTrade() {
        Trade t1 = new Trade("MSFT", 15, 205.37, TradeType.BUY);

        ResponseEntity<Trade> postResponse = restTemplate.postForEntity(getRootUrl() + "/trades", t1, Trade.class);
        assertNotNull(postResponse);
        assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdateTrade() {
        int id = 1;
        Trade trade = restTemplate.getForObject(getRootUrl() + "/trades/" + id, Trade.class);
        trade.setTicker("FB");


        restTemplate.put(getRootUrl() + "/trades/" + id, trade);

        Trade updatedTrade = restTemplate.getForObject(getRootUrl() + "/trades/" + id, Trade.class);
        assertNotNull(updatedTrade);
    }

    @Test
    public void testDeleteTrade() {
        int id = 2;
        Trade trade = restTemplate.getForObject(getRootUrl() + "/trades/" + id, Trade.class);
        assertNotNull(trade);

        restTemplate.delete(getRootUrl() + "/trades/" + id);

        try {
            trade = restTemplate.getForObject(getRootUrl() + "/trades/" + id, Trade.class);
        } catch (final HttpClientErrorException e) {
            assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
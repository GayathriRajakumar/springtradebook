package tradebook.mongo.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import tradebook.mongo.controller.TradeController.STATUS;
import tradebook.mongo.model.Trade;
import tradebook.mongo.repository.TradeRepository;

@Service
public class TradeService {

	@Autowired
	TradeRepository tradeRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(TradeService.class);

	public List<Trade> findAll() {

		Iterable<Trade> users = tradeRepository.findAll();
		logger.info("Trades of size " + tradeRepository.count() + " returned");
		return (List<Trade>) users;
	}

	public Long count() {

		logger.info("Trades of size " + tradeRepository.count() + " returned");
		return tradeRepository.count();
	}

	public void deleteById(String id) {

		logger.info("Trade of tickerName " + id + " deleted");
		tradeRepository.deleteById(id);
	}

	public Optional<Trade> findByTicker(String id) {
		Optional<Trade> usrAvailable = tradeRepository.findById(id);
		logger.info("Trade of tickerName " + id + " returned");
		return usrAvailable;
	}

	public Trade save(Trade trade) {
		logger.info("Trade of tickerName " + trade.getTicker() + " updated");
		return tradeRepository.save(trade);

	}
	
}

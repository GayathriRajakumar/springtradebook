package tradebook.mongo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tradebook.mongo.model.User;
import tradebook.mongo.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public List<User> findAll() {

		Iterable<User> users = userRepository.findAll();
		return (List<User>) users;
	}

	public Long count() {

		return userRepository.count();
	}

	public void deleteById(String id) {

		userRepository.deleteById(id);
	}

	public User findById(String id) {
		return userRepository.findByUserId(id);
	}
	
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	

	public User save(User user) {
		return userRepository.save(user);

	}
}

package tradebook.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import tradebook.mongo.model.User;

public interface UserRepository extends MongoRepository<User, String> {
	User findByUserId(String userId);
	User findByEmail(String email);
}

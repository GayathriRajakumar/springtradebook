package tradebook.mongo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import tradebook.mongo.model.Trade;
import tradebook.mongo.model.TradeState;

public interface TradeRepository extends MongoRepository<Trade, String> {
	public List<Trade> findByState(TradeState state);
}

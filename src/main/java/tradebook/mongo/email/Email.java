package tradebook.mongo.email;

import java.util.HashMap;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import tradebook.mongo.controller.TradeAngularController;
import tradebook.mongo.model.Trade;
import tradebook.mongo.service.TradeService;
import tradebook.mongo.service.UserService;

@Component
public class Email {

	private static final Logger logger = LoggerFactory.getLogger(Email.class);

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	UserService userService;
	@Autowired
	TradeService tradeService;

	TradeAngularController tradeController = new TradeAngularController();


	HashMap<String, Boolean> tradeMap = new HashMap<>();
	boolean sent = false;

	@Scheduled(fixedRate = 5000)
	@Transactional
	public void runSim() {

		String userID = TradeAngularController.idForMail;
		String tradeID = TradeAngularController.tradeIDForMail;

		if(!tradeMap.containsKey(tradeID)) {
			tradeMap.put(tradeID,sent);
		}
		logger.debug(tradeID +"  "+tradeMap.get(tradeID));
		if (userID != null && !userID.equals("") && tradeID!= null && !tradeID.equals("")
				&& Boolean.FALSE.equals(tradeMap.get(tradeID))) {
			System.out.println(sent);
			String name = userService.findById(userID).getName();
			String email = userService.findById(userID).getEmail();
			Optional<Trade> userTrade = tradeService.findByTicker(tradeID);
			if (userTrade.isPresent() && userTrade.get().getState().toString().equals("FILLED") ) {
				System.out.println(userTrade.get().getState());
				String status = "FILLED";
				String id = userTrade.get().getId();
				sendMail(name, email, id, status, userTrade.toString());
				tradeMap.replace(tradeID,true);
			}
			else if ( userTrade.isPresent() && userTrade.get().getState().toString().equals("REJECTED") ) {
				System.out.println(userTrade.get().getState());
				String status = "REJECTED";
				sendMail(name, email, tradeID, status, userTrade.toString());
				tradeMap.replace(tradeID,true);
			}
		}
	}

	public void sendMail (String name, String email, String id, String status, String subject)
	{
		logger.info("Sending mail to user....");
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);

		msg.setSubject("Spring TradeBook");
		msg.setText("Hi " + name + ", your trade " + id + "has been " + status + ". Here are the details: \n" + subject);


		javaMailSender.send(msg);
		logger.info("Mail delivery successful");
	}


}

package tradebook.mongo.model;

public class Response {
	
	private Object data;
	private String message;
	private int responseStatus;

	public Response() {
		
	}
	
	public Response(User data, String message, int responseStatus) {
		super();
		this.data = data;
		this.message = message;
		this.responseStatus = responseStatus;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(int responseStatus) {
		this.responseStatus = responseStatus;
	}
	
	

}

package tradebook.mongo.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class User {

	@Id
	private String userId;
	private String name;
	private String email;
	private String password;
	private double funds;
	private List<String> trades;
	
	public User(String id, String name, String email, String password) {
		super();
		this.userId = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.trades = new ArrayList<>();
	}

	public User() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

	public List<String> getTrades() {
		return trades;
	}

	public void setTrades(List<String> trades) {
		this.trades = trades;
	}

	@Override
	public String toString() {
		return "User [_id=" + userId + ", name=" + name + ", email=" + email + ", password=" + password + "]";
	}
	

}

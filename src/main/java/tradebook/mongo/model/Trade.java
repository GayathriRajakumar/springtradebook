package tradebook.mongo.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {


	@Id
	private String id;
	private String ticker;
	private int quantity;
	private double price;
	private Date created = new Date(System.currentTimeMillis());
	private TradeState state = TradeState.CREATED;
    private TradeType type ;

	public Trade(String ticker, int quantity, double price,TradeType type) {
		super();
		this.ticker = ticker;
		this.quantity = quantity;
		this.price = price;
		this.type =type;
	}

	public Trade() {


	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public TradeState getState() {
		return state;
	}

	public void setState(TradeState state) {
		this.state = state;
	}

	public TradeType getType() {
		return type;
	}

	public void setType(TradeType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Trade :" +"\n\n"+
				"ID ='" + id + "\n\n" +
				"ticker='" + ticker +"\n\n"+
				"quantity=" + quantity + "\n\n"+
				"price=" + price +"\n\n"+
				"created=" + created +"\n\n" +
				"state=" + state + "\n\n"+
				"type=" + type ;

	}

}

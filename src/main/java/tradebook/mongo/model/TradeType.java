package tradebook.mongo.model;

public enum TradeType {
    BUY("BUY"),
    SELL("SELL");

    private String type;

    private TradeType(String tradeType) {
        this.type = tradeType;
    }

    public String getTradeType() {
        return this.type;
    } 
}

package tradebook.mongo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tradebook.mongo.model.Response;
import tradebook.mongo.model.User;
import tradebook.mongo.service.UserService;

@RestController
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	JavaMailSender javaMailSender;
	
	public static  String idForMail="";

	@GetMapping("/user")
	public ResponseEntity<List<User>> getAllUsers() {
		try {
			List<User> users = new ArrayList<>();

			userService.findAll().forEach(users::add);

			if (users.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(users, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// find by id
	@GetMapping("/user/{id}")
	public ResponseEntity<User> getTradeById(@PathVariable("id") String id) {
		User tradeOptional = userService.findById(id);

		if (tradeOptional != null) {
			return new ResponseEntity<>(tradeOptional, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/login")
	public Response getTradeByEmail(@Valid @RequestBody User user) {
		User tradeOptional = userService.findByEmail(user.getEmail());

		if (tradeOptional != null) {
			if(tradeOptional.getPassword().equals(user.getPassword())) {
				return new Response(tradeOptional, "Success", 200);
			}
			else {
				return new Response(null, "Email or password incorrect.", 400);
			}
		} else {
			return new Response(null, "Email not yet registered.", 404);
		}
	}

	// count
	@GetMapping("/user/count")
	public Long count() {

		return userService.count();
	}

	// delete by id
	@DeleteMapping("/user/{id}")
	public void delete(@PathVariable String id) {

		userService.deleteById(id);
	}

	// create
	@PostMapping("/register")
	public Response createUser(@RequestBody User user) {
		
		User userOptional = userService.findByEmail(user.getEmail());

		if (userOptional != null) {
			return new Response(null, "Email already registered.", 400);
		} else {
			User newUser = userService.save(user);
			idForMail = newUser.getUserId();
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(user.getEmail());

			msg.setSubject("Spring TradeBook");
			msg.setText("Hi " + user.getName() + ". You have registered successfully for the Spring Tradebook by Group3-LOL");


			javaMailSender.send(msg);
			return new Response(newUser, "User registered successfully.", 200);
		}
	}

	// update
	@PutMapping("/user/{id}")
	public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable String id) {
		user.setUserId(id);
		if(userService.findById(id) == null)
			return ResponseEntity.notFound().build();
		else if(userService.findById(id) != null) {
			userService.save(user);
			return ResponseEntity.accepted().build();
		}
		else
			return ResponseEntity.badRequest().build();	
	}
	
	// add and delete funds
		@PutMapping("/user/{id}/addfunds")
		public ResponseEntity<Object> updateUserFunds(@RequestBody User user, @PathVariable String id) {
			user.setUserId(id);
			User newUser = userService.findById(id);
			if(newUser == null)
				return ResponseEntity.notFound().build();
			else {
				newUser.setFunds(user.getFunds());
				userService.save(newUser);
				return ResponseEntity.accepted().build();
			}
		}

	// error example for angular project
	@GetMapping("/error")
	public RuntimeException throwException() {
		return new RuntimeException("exception from user controller");

	}
}

package tradebook.mongo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import tradebook.mongo.model.Trade;
import tradebook.mongo.service.TradeService;

@Controller
@RequestMapping("/trade/")
public class TradeThymeleafController {

    @Autowired
    private TradeService tradeService;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(TradeThymeleafController.class);


    @GetMapping("newtrade")
    public String showSignUpForm(Trade trade) {
        return "newtrade";
    }

    @GetMapping("list")
    public String showUpdateForm(Model model) {
        model.addAttribute("trades", tradeService.findAll());
        return "alltrades";
    }

    @PostMapping("add")
    public String addTrade(@Valid Trade trade, BindingResult result, Model model) {
        if (result.hasErrors()) {
            System.out.println("if"+trade);
            return "newtrade";
        }
        System.out.println(trade);
        tradeService.save(trade);
        return "redirect:list";
    }



    @GetMapping("edit/{id}")
    public String showUpdateForm(@PathVariable("id") String id, Model model) {

        Trade trade = tradeService.findByTicker(String.valueOf(id))
                .orElseThrow(() -> new IllegalArgumentException("Invalid trade Id:" + id));
        System.out.println(trade.getId());
        model.addAttribute("trade", trade);
        return "update-trade";
    }

    @PostMapping("update/{id}")
    public String updateTrade(@PathVariable("id") String id, @Valid Trade trade, BindingResult result,
                              Model model) {
        trade.setId(id);
        System.out.println(trade.getQuantity());

        if (result.hasErrors()) {
            trade.setId(String.valueOf(id));
            return "update-trade";
        }

        tradeService.save(trade);
        model.addAttribute("trades", tradeService.findAll());
        return "alltrades";
    }

    @GetMapping("delete/{id}")
    public String deleteTrade(@PathVariable("id") String id, Model model) {
        tradeService.findByTicker(String.valueOf(id))
                .orElseThrow(() -> new IllegalArgumentException("Invalid trade Id:" + id));
        tradeService.deleteById(String.valueOf(id));
        model.addAttribute("trades", tradeService.findAll());
        return "alltrades";
    }

    // update
    @GetMapping("/trade/edit/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody Trade trade, @PathVariable String id) {

        logger.info("In TradeRepository updateTrade()");

        Optional<Trade> tradeOptional = tradeService.findByTicker(id);

        if (!tradeOptional.isPresent()) {
            logger.info("Trade of tickerName " + trade.getTicker() + " not present");
            return ResponseEntity.notFound().build();
        } else {
            trade.setId(id);
            tradeService.save(trade);
            return ResponseEntity.accepted().build();
        }
    }
}

package tradebook.mongo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;
import tradebook.mongo.exception.ResourceNotFoundException;
import tradebook.mongo.model.Response;
import tradebook.mongo.model.Trade;
import tradebook.mongo.model.TradeState;
import tradebook.mongo.model.User;
import tradebook.mongo.service.TradeService;
import tradebook.mongo.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin()
@RestController
@RequestMapping("/api/v1")
public class TradeAngularController {

	@Autowired
	private TradeService tradeService;
	@Autowired
	private UserService userService;

	@Autowired
	JavaMailSender javaMailSender;

	private static final Logger logger = LoggerFactory.getLogger(TradeAngularController.class);

	 public static  String idForMail="";
	public static  String tradeIDForMail="";

	@GetMapping("/trades")
	public List<Trade> getAllTrades(HttpServletRequest request) {
		logger.info(request.getRemoteAddr() + ": Get All Trades");
		return tradeService.findAll();
	}

	@GetMapping("/{uid}/trades")
	public List<Trade> getAllUserTrades(@PathVariable(value = "uid") String id,HttpServletRequest request) {
		User user = userService.findById(id);
		List<String> trades = user.getTrades();
		ArrayList<Trade> tradeList =  new ArrayList<>();
		if(trades != null) {
			for(String tradeId : trades) {
				tradeService.findByTicker(tradeId).ifPresent(tradeList::add);
			}
		}
		logger.info(request.getRemoteAddr() + ": Get all trades of " + id);
		return tradeList;
	}

	@GetMapping("/trades/{id}")
	public ResponseEntity<Trade> getTradeById(@PathVariable(value = "id") String id)
			throws ResourceNotFoundException {

		Trade trade = tradeService.findByTicker(id)
				.orElseThrow(() -> new ResourceNotFoundException("Trade not found for this id :: " + id));
		
		return ResponseEntity.ok().body(trade);
	}

	@PostMapping("/{uid}/trades")
	public Trade createTrade(@PathVariable(value = "uid") String id, @Valid @RequestBody Trade trade) {
		idForMail=id;

		Trade newTrade = tradeService.save(trade);
		User user = userService.findById(id);
		List<String> tradeList = user.getTrades();
		if(tradeList == null) {
			tradeList = new ArrayList<>();
		}
		tradeList.add(newTrade.getId());
		user.setTrades(tradeList);
		userService.save(user);
		tradeIDForMail= newTrade.getId();


		logger.info("Sending mail to user....");
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(user.getEmail());

		msg.setSubject("Spring TradeBook");
		msg.setText("Hi " + user.getName() + ", your trade " + newTrade.getId() + "has been created. Here are the details: \n" + newTrade.toString());


		javaMailSender.send(msg);
		logger.info("Mail delivery successful");


		return newTrade;
	}

	@PutMapping("/trades/{id}")
	public Response updateTrade(@PathVariable(value = "id") String id,
			@Valid @RequestBody Trade tradeDetails) throws ResourceNotFoundException {
		Response response = new Response();
		Trade trade = tradeService.findByTicker(id)
				.orElseThrow(() -> new ResourceNotFoundException("Trade not found for this id :: " + id));
		if(trade.getState().equals(TradeState.valueOf("FILLED")) || trade.getState().equals(TradeState.valueOf("REJECTED")) ) {
			response.setData(trade);
			response.setMessage("Filled or processed trade cannot be altered");
			response.setResponseStatus(405);
		}
		else {
			trade.setTicker(tradeDetails.getTicker());
			trade.setQuantity(tradeDetails.getQuantity());
			trade.setPrice(tradeDetails.getPrice());
			trade.setType(tradeDetails.getType());
			tradeService.save(trade);
			response.setData(trade);
			response.setMessage("Update success");
			response.setResponseStatus(200);
		}
		return response;
	}

	@DeleteMapping("/{uid}/trades/{id}")
	public Map<String, Boolean> deleteTrade(@PathVariable(value = "uid") String userId, @PathVariable(value = "id") String id){
		Map<String, Boolean> response = new HashMap<>();
		try {
		tradeService.findByTicker(id)
		.orElseThrow(() -> new ResourceNotFoundException("Trade not found for this id :: " + id));

		tradeService.deleteById(id);
		User user = userService.findById(userId);
		user.getTrades().remove(id);
		userService.save(user);
		response.put("deleted", Boolean.TRUE);
		}
		catch(ResourceNotFoundException e) {
			response.put(e.getMessage(), Boolean.FALSE);
		}
		return response;
	}
}
